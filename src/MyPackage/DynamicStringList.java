package MyPackage;


public class DynamicStringList implements SimpleList{

    private static final int Size = 5;
    private String[] mas;
    private int elem;

    public DynamicStringList() {
        this(Size);
    }

    public DynamicStringList(int size) {
        mas = new String[size];
    }

    @Override
    public void add(String s) {
        if(elem == mas.length){
            String[] newmas = new String[mas.length + 5];
            System.arraycopy(mas, 0, newmas, 0, elem);
            mas = newmas;
        }
        mas[elem++] = s;
    }

    @Override
    public String get() {
        try {
            if(elem == 0);
        } catch (Exception e) {
            System.out.println("The array is empty");
        }
        return mas[elem - 1];
    }

    @Override
    public String get(int id) {
        if (id < 0 || id >= elem) throw new IndexOutOfBoundsException("Incorrect index");
        return mas[id];
    }

    @Override
    public String remove() {
        try {
            if(elem == 0);
        } catch (Exception e) {
            System.out.println("The array is empty");
        }
        String masres = mas[elem - 1];
        elem--;
        return masres;
    }

    @Override
    public String remove(int id) {
        if (id < 0 || id >= elem) throw new IndexOutOfBoundsException("Incorrect index");
        String masdel = get(id);
        System.arraycopy(mas, id + 1, mas, id, mas.length - id - 1);
        elem--;
        return masdel;
    }

    @Override
    public boolean delete() {
        elem = 0;
        mas = new String[Size];
        return true;
    }

    @Override
    public String toString() {
        String masview = "";
        for (int i = 0; i < elem; i++) {
            masview += mas[i] + " ";
        }
        return masview;
    }

}
