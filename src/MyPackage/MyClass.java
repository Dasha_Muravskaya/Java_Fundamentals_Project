package MyPackage;

public class MyClass {
    public static void main(String[] args) {

        DynamicStringList obj = new DynamicStringList();

        obj.add("h");
        obj.add("e");
        obj.add("l");
        obj.add("l");
        obj.add("o");
        obj.add("k");
        obj.add("k");

        System.out.println(obj.toString());

        System.out.println("Getting a character with index 1: " + obj.get(1));
        System.out.println("Getting the last character: " + obj.get());

        System.out.println("Removable character with index 2: " + obj.remove(2));
        System.out.println(obj.toString());

        System.out.println("Delete the last character: " + obj.remove());
        System.out.println(obj.toString());

        System.out.println("Deleting all items: " + obj.delete());
        System.out.println(obj.toString());







    }
}
